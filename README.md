# Escritos

Textos que contiene este repositorio:

Nombre | Estado
-------|-------
«Idea y expresión en la propiedad intelectual» | 1.^a^ edición
«El (supuesto) vínculo entre el creador y lo creado» | 1.^a^ edición
«*Software*, dilapidación y ¿transgresión?» | 1.^a^ edición
«La persona humana y el mundo como información» | 1.^a^ edición
«La no-ficción en la ciencia ficción» | 1.^a^ edición
«Tecnologías de la información, sociedad y normatividad» | 1.^a^ edición
«Hegel contra Hegel» | 1.^a^ edición
«De la micro a la macropolítica de la propiedad intelectual» | 1.^a^ edición
«¿Propiedad intelectual antropológica» | 1.^a^ edición

## Licencia

Todos los escritos están bajo [Licencia Editorial Abierta y Libre (LEAL)](https://github.com/NikaZhenya/licencia-editorial-abierta-y-libre).
